﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;


namespace HanoiTowers
{
    public partial class FormMain : Form
    {
        int x1, x2, x3;
        int n, m = 0, p = 0;
        Timer timer1;
        Graphics graphics;
        Bitmap bitmap;
        Pen pen;
        int[,] box;
        char[,] move;
        SolidBrush solid;

        Timer timerGames;           // Таймер для игры
        int timerStart = 0;         // Время начала таймера
        int timerEnd = 30;          // Время окончания таймера
        bool isStart = false;       // Флаг старты игры
        bool isMoveDisk = false;    // Флаг переноса диска
        int numberMove = 0;         // Номер пирамиды, с которой был взят диск

        public FormMain()
        {
            InitializeComponent();
            // Записываем начальное значение таймера в тектовое поле на форме
            textBoxTimer.Text = timerEnd.ToString();
            // Для текущей формы (окна) задаим необходимо свойство
            // оно указывает на то, будет ли форма обрабатывать события
            // для отслеживания действяй с клавиатурой
            // Это необходимо для события KeyUp
            this.KeyPreview = true;
            // Вывод сообщения на форму
            labelMessage.Text = "Для начало игры наэмите <ENTER>";
        }

        /// <summary>
        /// Логика распределения
        /// </summary>
        /// <param name="h">Высота</param>
        private void hanoi(int h)
        {
            Stack<int> hA = new Stack<int>();
            Stack<int> hB = new Stack<int>();
            Stack<int> hC = new Stack<int>();
            while (h > 0)
            {
                hA.Push(h);
                h--;
            }
            char x = '3';
            char y = '1';
            while (hA.Count > 0)
            {
                if (hA.Count > 1)
                {
                    hC.Push(hA.Pop());
                    x = '1';
                    y = '3';
                    listBoxLogic.Items.Add(x + "-->" + y);
                    move[0, m] = x;
                    move[1, m] = y;
                    m++;
                }
                else
                {
                    hB.Push(hA.Pop());
                    x = '1';
                    y = '2';
                    listBoxLogic.Items.Add(x + "-->" + y);
                    move[0, m] = x;
                    move[1, m] = y;
                    m++;
                    while (hC.Count > 0)
                    {
                        hA.Push(hC.Pop());
                        x = '3';
                        y = '1';
                        listBoxLogic.Items.Add(x + "-->" + y);
                        move[0, m] = x;
                        move[1, m] = y;
                        m++;
                    }
                }
            }
            return;
        }

        private void RunAuto()
        {
            x1 = 0;
            x2 = 0;
            x3 = 0;
            n = 0;
            m = 0; 
            p = 0;
            timer1 = new System.Windows.Forms.Timer();
            timer1.Interval = 300;
            timer1.Tick += new EventHandler(timer1_Tick);


            int i;
            timer1.Enabled = true;
            listBoxLogic.Items.Clear();
            n = int.Parse(countDiskTextBox.Text);
            box = new int[3, n];
            move = new char[2, 3000];
            for (i = 0; i < n; i++)
            {
                box[0, i] = n - i;
            }
            hanoi(n);
            draw_();
            timer1.Enabled = true;
        }

        private void refr()
        {
            pictureBox1.Image = bitmap;
        }

        /// <summary>
        /// Происходит, когда отпускается клавиша, если элемент управления имеет фокус.
        /// Здесь отслеживаются нажатия на кнопки клавиатуры
        /// На форму вешается отслеживающее событие "KeyUp", 
        /// оно срабатывает момент отпускания любой клавиши
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_KeyUp(object sender, KeyEventArgs e)
        {
            // Првоверяем сатус игры
            if (isStart)
            {

                // Игра запущена


                // Срабатывает в момент нажатия клавиши 1
                if (e.KeyCode  == Keys.D1 || e.KeyCode  == Keys.NumPad1)
                {
                    // Меняем сообщение
                    if (isMoveDisk)
                    {
                        labelMessage.Text = "Нажмите цифру в соответствии с номером башни с которой хотите переложить диск";

                        // Выполняем расчет для матрице box
                        calculation();

                        switch (numberMove)
                        {
                            case 2:
                                box[0, x1] = box[1, x2 - 1];
                                box[1, x2 - 1] = 0;
                                break;
                            case 3:
                                box[0, x1] = box[2, x3 - 1];
                                box[2, x3 - 1] = 0;
                                break;
                            default:
                                break;
                        }
                        

                        draw_();
                        refr();
                        numberMove = 0;         // Сбрасываем номер пирамиды, с которой взяли диск
                        isMoveDisk = false;     // Сбрасываем флаг взятие диска
                        if (checkResult())
                        {
                            timerGames.Stop();
                            panelMessage.BackColor = Color.LightGreen;
                            labelMessage.Text = "Вы победили!";
                            MessageBox.Show("Вы победили!");
                            Environment.Exit(0);
                        }
                    }
                    else
                    {
                        // Провекра наличие дисков на парамиде
                        if (box[0, 0] == 0)
                        {
                            MessageBox.Show("На данной пирамиде нет дисков!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            labelMessage.Text = "Взят диск с пирамиды 1, куда его переложить?";
                            numberMove = 1;
                            isMoveDisk = true;
                        }
                    }
                }

                // Срабатывает в момент нажатия клавиши 2
                if (e.KeyCode == Keys.D2 || e.KeyCode == Keys.NumPad2)
                {
                    // Меняем сообщение
                    if (isMoveDisk)
                    {
                        labelMessage.Text = "Нажмите цифру в соответствии с номером башни с которой хотите переложить диск";


                        // Выполняем расчет для матрице box
                        calculation();

                        switch (numberMove)
                        {
                            case 1:
                                box[1, x2] = box[0, x1 - 1];
                                box[0, x1 - 1] = 0;
                                break;
                            case 3:
                                box[1, x2] = box[2, x3 - 1];
                                box[2, x3 - 1] = 0;
                                break;
                            default:
                                break;
                        }


                        draw_();
                        refr();
                        numberMove = 0;         // Сбрасываем номер пирамиды, с которой взяли диск
                        isMoveDisk = false;     // Сбрасываем флаг взятие диска
                        if (checkResult())
                        {
                            timerGames.Stop();
                            panelMessage.BackColor = Color.LightGreen;
                            labelMessage.Text = "Вы победили!";
                            MessageBox.Show("Вы победили!");
                            Environment.Exit(0);
                        }
                    }
                    else
                    {
                        // Провекра наличие дисков на парамиде
                        if (box[1, 0] == 0)
                        {
                            MessageBox.Show("На данной пирамиде нет дисков!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            labelMessage.Text = "Взят диск с пирамиды 2, куда его переложить?";
                            numberMove = 2;
                            isMoveDisk = true;
                        }
                    }
                }

                // Срабатывает в момент нажатия клавиши 3
                if (e.KeyCode == Keys.D3 || e.KeyCode == Keys.NumPad3)
                {
                    // Меняем сообщение
                    if (isMoveDisk)
                    {
                        labelMessage.Text = "Нажмите цифру в соответствии с номером башни с которой хотите переложить диск";

                        // Выполняем расчет для матрице box
                        calculation();

                        switch (numberMove)
                        {
                            case 1:
                                box[2, x3] = box[0, x1 - 1];
                                box[0, x1 - 1] = 0;
                                break;
                            case 2:
                                box[2, x3] = box[1, x2 - 1];
                                box[1, x2 - 1] = 0;
                                break;
                            default:
                                break;
                        }


                        draw_();
                        refr();
                        numberMove = 0;         // Сбрасываем номер пирамиды, с которой взяли диск
                        isMoveDisk = false;     // Сбрасываем флаг взятие диска
                        if (checkResult())
                        {
                            timerGames.Stop();
                            panelMessage.BackColor = Color.LightGreen;
                            labelMessage.Text = "Вы победили!";
                            MessageBox.Show("Вы победили!");
                            Environment.Exit(0);
                        }
                    }
                    else
                    {
                        // Провекра наличие дисков на парамиде
                        if (box[2, 0] == 0)
                        {
                            MessageBox.Show("На данной пирамиде нет дисков!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            labelMessage.Text = "Взят диск с пирамиды 3, куда его переложить?";
                            numberMove = 3;
                            isMoveDisk = true;
                        }
                    }
                }
            }
            else
            {
                // Игра остановлена

                // Срабатывает в момент нажатия клавиши Enter
                if ((e.KeyCode & Keys.Enter) == Keys.Enter)
                {
                    countDiskTextBox.Enabled = false;
                    // Запуск таймера
                    SetTimer();

                    // Меняем сообщение
                    labelMessage.Text = "Нажмите цифру в соответствии с номером башни с которой хотите переложить диск";

                    // Меняем флаг игры
                    isStart = true;

                    x1 = 0;
                    x2 = 0;
                    x3 = 0;
                    n = 0;
                    m = 0;
                    p = 0;

                    n = int.Parse(countDiskTextBox.Text);
                    box = new int[3, n];
                    move = new char[2, 3000];
                    for (int i = 0; i < n; i++)
                    {
                        box[0, i] = n - i;
                    }
                    hanoi(n);
                    graphics.Clear(Color.White);

                    x1 = -1;
                    x2 = -1;
                    x3 = -1;
                    graphics.DrawLine(pen, 60, 350, 200, 350);
                    graphics.DrawLine(pen, 250, 350, 390, 350);
                    graphics.DrawLine(pen, 440, 350, 580, 350);
                    graphics.DrawLine(pen, 130, 350, 130, 150);
                    graphics.DrawLine(pen, 320, 350, 320, 150);
                    graphics.DrawLine(pen, 510, 350, 510, 150);
                    for (int i = 0; i < n; i++)
                    {
                        if (box[0, i] == 0)
                        {
                            x1 = i;
                            break;
                        }
                    }
                    for (int i = 0; i < n; i++)
                    {
                        if (box[1, i] == 0)
                        {
                            x2 = i;
                            break;
                        }
                    }
                    for (int i = 0; i < n; i++)
                    {
                        if (box[2, i] == 0)
                        {
                            x3 = i;
                            break;
                        }
                    }
                    if (x1 == -1) x1 = n;
                    if (x2 == -1) x2 = n;
                    if (x3 == -1) x3 = n;
                    draw_();
                    refr();
                    n = int.Parse(countDiskTextBox.Text);

                }
            }
        }

        /// <summary>
        /// Выполняет расчет взятого диска в матрице box
        /// </summary>
        private void calculation()
        {
            graphics.Clear(Color.White);
            x1 = -1;
            x2 = -1;
            x3 = -1;
            graphics.DrawLine(pen, 60, 350, 200, 350);
            graphics.DrawLine(pen, 250, 350, 390, 350);
            graphics.DrawLine(pen, 440, 350, 580, 350);
            graphics.DrawLine(pen, 130, 350, 130, 150);
            graphics.DrawLine(pen, 320, 350, 320, 150);
            graphics.DrawLine(pen, 510, 350, 510, 150);
            for (int i = 0; i < n; i++)
            {
                if (box[0, i] == 0)
                {
                    x1 = i;
                    break;
                }
            }
            for (int i = 0; i < n; i++)
            {
                if (box[1, i] == 0)
                {
                    x2 = i;
                    break;
                }
            }
            for (int i = 0; i < n; i++)
            {
                if (box[2, i] == 0)
                {
                    x3 = i;
                    break;
                }
            }
            if (x1 == -1) x1 = n;
            if (x2 == -1) x2 = n;
            if (x3 == -1) x3 = n;
        }

        /// <summary>
        /// Проверяет результат
        /// </summary>
        /// <returns></returns>
        private bool checkResult()
        {
            bool s = true;
            for (int i = 0; i < n; i++)
            {
                if (box[1, i] != n - i)
                {
                    s = false;
                    break;
                }

            }
            if (s)
            {

                return true;
            }
            s = true;
            for (int i = 0; i < n; i++)
            {

                if (box[2, i] != n - i)
                {
                    s = false;
                    break;
                }
            }
            return s;
        }

        private int pow(int a, int b)
        {
            if (b != 0)
                return a * pow(a, b - 1);
            else
                return 1;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            countDiskTextBox.Text = "3";
            pictureBox1.BackColor = Color.White;
            timer1 = new System.Windows.Forms.Timer();
            timer1.Interval = 300;
            timer1.Tick += new EventHandler(timer1_Tick);
            bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            graphics = Graphics.FromImage(bitmap);
            pen = new Pen(Color.Black, 3);
        }

        private void draw_()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (box[i, j] != 0)
                    {
                        switch (box[i, j])
                        {
                            case 0: solid = new SolidBrush(Color.Red); break;
                            case 1: solid = new SolidBrush(Color.Pink); break;
                            case 2: solid = new SolidBrush(Color.Yellow); break;
                            case 3: solid = new SolidBrush(Color.Green); break;
                            case 4: solid = new SolidBrush(Color.Blue); break;
                            case 5: solid = new SolidBrush(Color.Brown); break;
                            case 6: solid = new SolidBrush(Color.DarkRed); break;
                            case 7: solid = new SolidBrush(Color.DarkGreen); break;
                            case 8: solid = new SolidBrush(Color.DarkOrange); break;
                            case 9: solid = new SolidBrush(Color.DarkGreen); break;
                            case 10: solid = new SolidBrush(Color.DarkBlue); break;
                        }
                        graphics.FillRectangle(solid, (i + 1) * 190 - 60 - (box[i, j]) * 10, 320 - j * 30, box[i, j] * 20, 30);
                    }
                }

            }
        }

        void timer1_Tick(object sender, EventArgs e)
        {

            graphics.Clear(Color.White);

            x1 = -1;
            x2 = -1;
            x3 = -1;

            graphics.DrawLine(pen, 60, 350, 200, 350);
            graphics.DrawLine(pen, 250, 350, 390, 350);
            graphics.DrawLine(pen, 440, 350, 580, 350);
            graphics.DrawLine(pen, 130, 350, 130, 150);
            graphics.DrawLine(pen, 320, 350, 320, 150);
            graphics.DrawLine(pen, 510, 350, 510, 150);

            for (int i = 0; i < n; i++)
            {
                if (box[0, i] == 0)
                {
                    x1 = i;
                    break;
                }
            }


            for (int i = 0; i < n; i++)
            {
                if (box[1, i] == 0)
                {
                    x2 = i;
                    break;
                }
            }
            for (int i = 0; i < n; i++)
            {
                if (box[2, i] == 0)
                {
                    x3 = i;
                    break;
                }
            }
            if (x1 == -1) x1 = n;
            if (x2 == -1) x2 = n;
            if (x3 == -1) x3 = n;

            if (x1 == 0 && x3 == 0)
            {
                timer1.Stop();
                MessageBox.Show("Построение завершенно");
            }


            switch (move[0, p])
            {
                case 'A':
                    switch (move[1, p])
                    {
                        case 'B':
                            box[1, x2] = box[0, x1 - 1];
                            box[0, x1 - 1] = 0;
                            break;
                        case 'C':
                            box[2, x3] = box[0, x1 - 1];
                            box[0, x1 - 1] = 0;
                            break;
                    }
                    break;
                case 'B':
                    switch (move[1, p])
                    {
                        case 'A':
                            box[0, x1] = box[1, x3 - 1];
                            box[2, x3 - 1] = 0;
                            break;
                        case 'B':
                            box[1, x2] = box[2, x3 - 1];
                            box[2, x3 - 1] = 0;
                            break;
                    }
                    break;
                case 'C':
                    switch (move[1, p])
                    {
                        case 'A':
                            box[0, x1] = box[2, x3 - 1];
                            box[2, x3 - 1] = 0;
                            break;
                        case 'B':
                            box[1, x2] = box[2, x3 - 1];
                            box[2, x3 - 1] = 0;
                            break;
                    }
                    break;
            }
            draw_();
            refr();
            p++;
        }

        /// <summary>
        /// Запускаем таймер с шагом 1 сек.
        /// </summary>
        private void SetTimer()
        {
            timerGames = new Timer();
            timerGames.Interval = 1000;
            timerGames.Tick += new EventHandler(timerGames_Tick);
            timerGames.Enabled = true;
            //timerGames.Enabled = true;
        }
        /// <summary>
        /// Шаг таймера
        /// </summary>
        void timerGames_Tick(object sender, EventArgs e)
        {
            timerStart++;
            textBoxTimer.Text = (timerEnd - timerStart).ToString();
            if (timerStart == timerEnd)
            {
                timerGames.Stop();
                panelMessage.BackColor = Color.LightCoral;
                labelMessage.Text = "Время вышло, вы проиграли.";
                MessageBox.Show("Время вышло, вы проиграли");
                Environment.Exit(0);
            }
        }
    }
}
